# PalldeOCR_cppdll

#### 介绍
我在使用PaddleOCR时想得到推理结果，但官方没有提供相关方法，网上也没有白嫖到代码（嘿嘿），所以自己动手，有了这个仓库

#### 编译
1. 把ocr_export.cpp, ocr_rec.cpp复制到deploy\cpp_infer\src中，ocr_export.h, ocr_rec.h复制到deploy\cpp_infer\include中，CMakeLists.txt覆盖deploy\cpp_infer中的原来CMakeLists.txt文件
2. 使用Cmake编译，具体的教程可以在官方的文档里找到<https://gitee.com/paddlepaddle/PaddleOCR/blob/release/2.4/deploy/cpp_infer/readme.md#/paddlepaddle/PaddleOCR/blob/release/2.4/deploy/cpp_infer/docs/windows_vs2019_build.md>
3. 生成

#### 使用方法
- 代码提供了一个TextOCR类，推理前需要先调用TextOCR::InitTextOCR初始化推理时用到的参数，才能调用TextOCR::PaddleOCRText方法进行推理
- TextOCR::PaddleOCRText方法返回一个char\*类型的字符串，每个结果间用 \n 符分割
- 因为我没有输出日志到文件或是返回推理时间的需求，所以没有写相关代码，有需要的可以参考deploy\cpp_infer\src\mian.cpp内的main_system方法，我的实现流程便是其简化版本
- 使用：包含ocr_export.h，链接ocr_export.lib就能编译了