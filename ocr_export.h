#ifndef OCR_EXPORT
#define OCR_EXPORT
#include <iostream>
#include <direct.h>  
#include <stdio.h> 
#include <codecvt> 
#include <fstream>
#include <string>
#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <include/ocr_det.h>
#include <include/ocr_rec.h>
#include <include/ocr_cls.h>

// 需将ppocr/utils/ppocr_keys_v1.txt移动到当前目录下
#ifdef PaddleOCRExport_EXPORTS
#define OCRAPI __declspec(dllexport)
#else
#define OCRAPI __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C" {
#endif
	namespace assistant
	{
		class OCRAPI TextOCR
		{
		public:
			void InitTextOCR(const char* save_log_path, bool use_gpu,
				int gpu_id, int gpu_mem, int cpu_threads,
				bool enable_mkldnn, bool use_tensorrt, const char* precision,
				bool benchmark, const char* det_model_dir, int max_side_len,
				double det_db_thresh, double det_db_box_thresh, double det_db_unclip_ratio,
				bool use_polygon_score, bool visualize, bool use_angle_cls,
				const char* cls_model_dir, double cls_thresh, const char* rec_model_dir,
				int rec_batch_num, const char* pchar_list_file);
			char* PaddleOCRText(cv::Mat& image);
			
		private:
			const char* _save_log_path;
			bool _use_gpu;
			int _gpu_id;
			int _gpu_mem;
			int _cpu_threads;
			bool _enable_mkldnn;
			bool _use_tensorrt;
			const char* _precision;
			bool _benchmark;
			const char* _det_model_dir;
			int _max_side_len;
			double _det_db_thresh;
			double _det_db_box_thresh;
			double _det_db_unclip_ratio;
			bool _use_polygon_score;
			bool _visualize;
			bool _use_angle_cls;
			const char* _cls_model_dir;
			double _cls_thresh;
			const char* _rec_model_dir;
			int _rec_batch_num;
			const char* _pchar_list_file;
		};
	}

#ifdef __cplusplus
}
#endif


#endif // !OCR_EXPORT
